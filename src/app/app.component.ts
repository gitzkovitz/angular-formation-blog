import { Component } from '@angular/core';
import {Post} from "./post-list-item/post";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'blog';

  posts = [
      new Post(
        'Mon 1er post',
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer pretium mauris at lobortis tincidunt. Nam tincidunt orci nec placerat tempor. Cras ac elit id neque bibendum commodo eget in nisi. Donec dapibus bibendum posuere. Proin gravida arcu a sapien pellentesque efficitur. Etiam faucibus velit nec sapien ultricies, a bibendum nisl lobortis. Proin sit amet quam ac risus lacinia laoreet at sed arcu. Vivamus leo arcu, aliquet non tincidunt nec, porttitor at lorem. Donec pellentesque neque quis risus accumsan ultrices. Proin risus augue, efficitur vitae est vel, vulputate facilisis neque. Aenean in nibh placerat, egestas nunc eu, mattis lorem.'
      ),
      new Post(
        'Mon 2ème post',
        'Maecenas egestas velit nec sem molestie viverra. Nulla eu blandit nisi, vitae porttitor dui. Aliquam malesuada eget sem et scelerisque. Quisque luctus fringilla congue. Etiam fringilla tortor mi, ut tincidunt risus aliquet ultrices. In consectetur nulla lacus, at porttitor diam lacinia nec. Sed pretium rutrum augue quis maximus. Donec non fringilla arcu, consequat accumsan massa. Donec mattis sed eros ac porta.'
      ),
      new Post(
        'Mon 3ème post',
        'Pellentesque vitae mi augue. Curabitur imperdiet ultrices semper. Nullam quis ultrices arcu, tincidunt imperdiet orci. Nulla imperdiet aliquam mauris ac dapibus. Nullam pellentesque suscipit turpis, ut pulvinar magna iaculis et. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nulla et diam quam.'
      )
  ];
}
